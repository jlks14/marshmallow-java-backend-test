# To Build

```
cd $PROJECT_ROOT && mvn clean package
```

# To Run

```
cd $PROJECT_ROOT && java -jar target/java-backend-0.0.1-SNAPSHOT.jar
```

# Test the Endpoint (original example input)

```
curl -X POST \
  http://localhost:8080/clean \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "areaSize" : [5, 5],
  "startingPosition" : [1, 4],
  "oilPatches" : [
    [1, 0],
    [2, 2],
    [2, 3]
  ],
  "navigationInstructions" : "NNESEESWNWW"
}'
```