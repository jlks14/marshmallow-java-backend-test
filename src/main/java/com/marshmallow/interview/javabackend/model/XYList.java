package com.marshmallow.interview.javabackend.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class XYList extends ArrayList<Integer> {

    public static final int X = 0;
    public static final int Y = 1;
    
    public XYList(int x, int y){
        this.add(x);
        this.add(y);
    }

    @JsonIgnore
    public int getX() {
        return this.get(0);
    }

    @JsonIgnore
    public int getY() {
        return this.get(1);
    }

    @JsonIgnore
    public int setX(int x){
        return this.set(X, x);
    }

    @JsonIgnore
    public int setY(int y){
        return this.set(Y, y);
    }

}