package com.marshmallow.interview.javabackend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SeaCleanerOutput {

    private Position finalPosition;

    private Integer oilPatchesCleaned;

    private Exception error;

    public SeaCleanerOutput(Position finalPosition, Integer oilPatchesCleaned ){
        this.finalPosition = finalPosition;
        this.oilPatchesCleaned = oilPatchesCleaned;
    }

    public SeaCleanerOutput(Exception error){
        this.error = error;
    }

    public boolean hasError(){
        return error != null;
    }
    
}