package com.marshmallow.interview.javabackend.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SeaCleanerInput {
    
    private AreaSize areaSize;
    
    private Position startingPosition;

    private List<Position> oilPatches;
    
    private String navigationInstructions;

}