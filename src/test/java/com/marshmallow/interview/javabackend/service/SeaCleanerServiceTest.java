package com.marshmallow.interview.javabackend.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.marshmallow.interview.javabackend.model.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeaCleanerServiceTest {

    @Autowired
    private SeaCleanerService service;

    @Test
    public void testCleaningTheSea(){
        List<Position> oilPatches = new ArrayList<>();
        oilPatches.add(new Position(1, 0));
        oilPatches.add(new Position(2, 2));
        oilPatches.add(new Position(2, 3));

        SeaCleanerInput input = new SeaCleanerInput(new AreaSize(5, 5), new Position(1,2), oilPatches, "NNESEESWNWW");
        SeaCleanerOutput output = service.cleanTheSea(input); 
        
        assertEquals(false, output.hasError());

        assertEquals(new Integer(1), output.getOilPatchesCleaned());

        assertEquals(1, output.getFinalPosition().getX());
        assertEquals(3, output.getFinalPosition().getY());
    }

    @Test
    public void testCleaningTheSeaFailure(){
        List<Position> oilPatches = new ArrayList<>();
        oilPatches.add(new Position(1, 0));
        oilPatches.add(new Position(2, 2));
        oilPatches.add(new Position(2, 3));

        SeaCleanerInput input = new SeaCleanerInput(new AreaSize(5, 5), new Position(4,4), oilPatches, "NNESEESWNWW");
        SeaCleanerOutput output = service.cleanTheSea(input); 
        
        assertEquals(true, output.hasError());
    }

}