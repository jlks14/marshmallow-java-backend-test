package com.marshmallow.interview.javabackend.model;

import lombok.Getter;

@Getter
public enum Direction {
    S ("S", 1, -1),
    N ("N", 1, 1),
    E ("E", 0, 1),
    W ("W", 0, -1);

    private String letter;
    private int index;
    private int movement;

    Direction (String letter, int index, int movement){
        this.letter = letter;
        this.index = index;
        this.movement = movement;
    }


}