package com.marshmallow.interview.javabackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.marshmallow.interview.javabackend.model.*;
import com.marshmallow.interview.javabackend.service.SeaCleanerService;

@RestController
@RequestMapping(value = "/clean")
public class SeaCleanerController {

    @Autowired
    SeaCleanerService service;

    @PostMapping
    public ResponseEntity<SeaCleanerOutput> cleanTheSea(@RequestBody SeaCleanerInput input){
        SeaCleanerOutput output = service.cleanTheSea(input);

        if(output.hasError()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(output);
        }

        return ResponseEntity.ok(output);
    }

}