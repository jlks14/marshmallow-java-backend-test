package com.marshmallow.interview.javabackend.model;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AreaSize extends XYList {
    
    public AreaSize(int x, int y){
        super(x, y);
    }

    @Override
    public AreaSize clone(){
        return new AreaSize(this.getX(), this.getY());
    }

}