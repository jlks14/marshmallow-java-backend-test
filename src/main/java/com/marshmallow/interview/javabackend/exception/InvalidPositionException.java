package com.marshmallow.interview.javabackend.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.marshmallow.interview.javabackend.model.Position;

import lombok.Getter;

@Getter
@JsonIgnoreProperties(value = {
    "stackTrace",
    "suppressed",
    "localizedMessage"
})
@JsonInclude(Include.NON_NULL)
public class InvalidPositionException extends Exception {

    private Position invalidPosition;

    public InvalidPositionException(Position position) {
        super("Invalid Position");
        this.invalidPosition = position;
    }

}