package com.marshmallow.interview.javabackend.model;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Position extends XYList {

    public Position(int x, int y){
        super(x, y);
    }

    @Override
    public Position clone(){
        return new Position(this.getX(), this.getY());
    }

}