package com.marshmallow.interview.javabackend.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.marshmallow.interview.javabackend.exception.InvalidPositionException;
import com.marshmallow.interview.javabackend.model.*;

@Service
public class SeaCleanerService {
    
    public SeaCleanerOutput cleanTheSea(SeaCleanerInput input){
        return this.cleanTheSea(input.getAreaSize(), input.getStartingPosition(), input.getNavigationInstructions(), input.getOilPatches());
    }

    public SeaCleanerOutput cleanTheSea(AreaSize size, Position start, String navigationInstructions, List<Position> oilPatches) {
        
        List<Direction> directionList = Arrays.asList(navigationInstructions.split(""))
        .stream()
        .map((letter)->{
            return Direction.valueOf(letter);
        })
        .collect(Collectors.toList());

        return this.cleanTheSea(size, start, directionList, oilPatches);
    }
    
    public SeaCleanerOutput cleanTheSea(AreaSize size, Position start, List<Direction> directions, List<Position> oilPatches) {
        
        try {
            int oilPatchCount=0;
        
            Position currentPosition = start;
            
            for(int i=0; i < directions.size(); i++){   
                currentPosition = this.getNextPosition(size, currentPosition, directions.get(i));
    
                int indexOfPosition = this.indexOfPosition(oilPatches, currentPosition);
                
                if( indexOfPosition > -1 ){
                    oilPatchCount+=1;
                    oilPatches.remove(indexOfPosition);
                }
            }
    
            return new SeaCleanerOutput(currentPosition, oilPatchCount);
        } catch(InvalidPositionException e) {
            return new SeaCleanerOutput(e);
        }

    }

    private Position getNextPosition(AreaSize size, Position currentPosition, Direction direction)
            throws InvalidPositionException {
        
        Position newPosition = currentPosition.clone();

        newPosition.set(direction.getIndex(), newPosition.get(direction.getIndex()) + direction.getMovement());

        if(!this.isPositionValid(size, newPosition)){
            throw new InvalidPositionException(newPosition);
        }

        return newPosition;
    }

    private int indexOfPosition(List<Position> oilPatches, Position position){
        for(int i=0; i < oilPatches.size(); i++){
            Position p = oilPatches.get(i);

            if(p.getX() == position.getX() && p.getY() == position.getY()){
                return i; 
            }
        }

        return -1;
    }

    private boolean isPositionValid(AreaSize size, Position position){
        if( position.getX() < 0 || position.getX() > (size.getX()-1) ){
            return false;
        } else if( position.getY() < 0 || position.getY() > (size.getY()-1) ){
            return false;
        }

        return true;
    }

}